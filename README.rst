********************************************************************************
RPNCalculator: Calculation of math expression using Reverse Polish Notation.
********************************************************************************

============
Installation
============

To install **RPNCalculator** you should download the tarball, unpack it and run the *setup.py* script

.. code-block:: bash

    $ python2 setup.py install

=====
Usage
=====

You can use class **RPNCalculator** to calculate math expressions:

.. code-block:: python

    >>> from RPNCalc import rpn
    >>> calculator = rpn.RPNCalculator()
    >>> calculator.calculate("1 + 2*3")
    Result: 7.0

Also you can initialize **RPNCalculator** with your own module with functions and constants.
You only need to send the name of module as an argument when you create an instance of a class:

.. code-block:: python

    >>> from RPNCalc import rpn
    >>> calculator = rpn.RPNCalculator(["math", "your_module"])
    >>> calculator.calculate("sin(pi) + your_function(your_const + e)")


Using bash shell:

.. code-block:: bash

    $ calc 'log10(100)'
    Result: 2.0
