#!/usr/bin/env python2
import unittest
import rpn
from math import *


class OKTest(unittest.TestCase):
    calculator = rpn.RPNCalculator().calculate

    def test_unary_operators(self):
        self.assertEqual(-13, self.calculator("-13"))
        self.assertEqual(19, self.calculator("6-(-13)"))
        self.assertEqual(0, self.calculator("1---1"))
        self.assertEqual(-1, self.calculator("-+---+-1"))

    def test_operation_priority(self):
        self.assertEqual(5, self.calculator("1+2*2"))
        self.assertEqual(25, self.calculator("1+(2+3*2)*3"))
        self.assertEqual(30, self.calculator("10*(2+1)"))
        self.assertEqual(1000, self.calculator("10**(2+1)"))
        self.assertEqual(100 // 3 ** 2, self.calculator("100//3**2"))
        self.assertEqual(100 // 3 % 2 ** 2, self.calculator("100//3%2**2"))

    def test_functions_and_constants(self):
        self.assertEqual(pi + e, self.calculator("pi+e"))
        self.assertEqual(1, self.calculator("log(e)"))
        self.assertEqual(1, self.calculator("sin(pi/2)"))
        self.assertEqual(2, self.calculator("log10(100)"))
        self.assertEqual(666, self.calculator("sin(pi/2)*111*6"))
        self.assertEqual(2, self.calculator("2*sin(pi/2)"))

    def test_associative(self):
        self.assertEqual(102 % 12 % 7, self.calculator("102%12%7"))
        self.assertEqual(100 // 4 // 3, self.calculator("100//4//3"))
        self.assertEqual(2 ** 3 ** 4, self.calculator("2**3**4"))

    def test_comparison_operators(self):
        self.assertEqual(1 + 2 * 3 == 1 + 2 * 3, self.calculator("1+2*3==1+2*3"))
        self.assertEqual(e ** 5 >= e ** 5 + 1, self.calculator("e**5>=e**5+1"))
        self.assertEqual(1 + 2 * 4 // 3 + 1 != 1 + 2 * 4 // 3 + 2, self.calculator("1+2*4//3+1!=1+2*4//3+2"))

    def test_common_tests(self):
        self.assertEqual(100, self.calculator("(100)"))
        self.assertEqual(666, self.calculator("666"))
        self.assertEqual(30, self.calculator("10(2+1)"))
        self.assertEqual(-0.1, self.calculator("-.1"))
        self.assertEqual(1. / 3, self.calculator("1/3"))
        self.assertEqual(1.0/3.0, self.calculator("1.0/3.0"))
        self.assertEqual(.1 * 2.0**56.0, self.calculator(".1 * 2.0**56.0"))
        self.assertEqual(e**34, self.calculator("e**34"))
        self.assertEqual((2.0**(pi/pi+e/e+2.0**0.0)), self.calculator("(2.0**(pi/pi+e/e+2.0**0.0))"))
        self.assertEqual((2.0**(pi/pi+e/e+2.0**0.0))**(1.0/3.0), self.calculator("(2.0**(pi/pi+e/e+2.0**0.0))**(1.0/3.0)"))
        self.assertEqual(sin(pi/2**1) + log(1*4+2**2+1, 3**2), self.calculator("sin(pi/2**1) + log(1*4+2**2+1, 3**2)"))

        self.assertEqual(10*e**0.*log10(.4* -5/ -0.1-10) - -abs(-53//10) + -5, self.calculator("10*e^0*log10(.4* -5/ -0.1-10) - -abs(-53//10) + -5"))
        self.assertEqual(sin(-cos(-sin(3.0)-cos(-sin(-3.0*5.0)-sin(cos(log10(43.0))))+cos(sin(sin(34.0-2.0**2.0))))--cos(1.0)--cos(0.0)**3.0),
                         self.calculator("sin(-cos(-sin(3.0)-cos(-sin(-3.0*5.0)-sin(cos(log10(43.0))))+cos(sin(sin(34.0-2.0**2.0))))--cos(1.0)--cos(0.0)**3.0)"))
        self.assertEqual(2.0**(2.0**2.0*2.0**2.0), self.calculator("2.0**(2.0**2.0*2.0**2.0)"))
        self.assertEqual(sin(e**log(e**e**sin(23.0),45.0) + cos(3.0+log10(e**-e))), self.calculator("sin(e**log(e**e**sin(23.0),45.0) + cos(3.0+log10(e**-e)))"))


class RaisesTest(unittest.TestCase):
    calculator = rpn.RPNCalculator().calculate

    def runTest(self):
        self.assertRaises(Exception, self.calculator, "")
        self.assertRaises(Exception, self.calculator, "+")
        self.assertRaises(Exception, self.calculator, "1-")
        self.assertRaises(Exception, self.calculator, "1 2")
        self.assertRaises(Exception, self.calculator, "ee")
        self.assertRaises(Exception, self.calculator, "123e")
        self.assertRaises(Exception, self.calculator, "==7")
        self.assertRaises(Exception, self.calculator, "1 * * 2")
        self.assertRaises(Exception, self.calculator, "1 + 2(3 * 4))")
        self.assertRaises(Exception, self.calculator, "((1+2)")
        self.assertRaises(Exception, self.calculator, "1 + 1 2 3 4 5 6 ")
        self.assertRaises(Exception, self.calculator, "log100(100)")
        self.assertRaises(Exception, self.calculator, "------")
        self.assertRaises(Exception, self.calculator, "5 > = 6")
        self.assertRaises(Exception, self.calculator, "5 / / 6")
        self.assertRaises(Exception, self.calculator, "6 < = 6")
        self.assertRaises(Exception, self.calculator, "6 * * 6")
        self.assertRaises(Exception, self.calculator, "(((((")


if __name__ == '__main__':
    unittest.main()
