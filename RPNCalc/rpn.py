""" This module allows to calculate mathematical expressions.
Calculation of math expression occur via Reverse Polish Notation.
For more information view help(rpn.RPNCalculator) """

import importlib
from collections import namedtuple
from numbers import Number
from config import floating_point, open_parenthis, close_parenthis
from string import whitespace

# universal unit for the mathematical operation
Operation = namedtuple("Operation", "func priority is_unary left_ass")


def is_function(obj):
    return hasattr(obj, '__call__')


def is_number(arg):
    if arg is None:
        return False
    elif type(arg) == str or type(arg) == unicode:
        return arg.replace(floating_point, '', 1).isdigit()
    elif isinstance(arg, Number):
        return True
    else:
        return False


def is_parenthesis(symbol):
    return symbol == open_parenthis or symbol == close_parenthis


def get_constants_from_module(module):
    """
    Returns dictionary with constants names as keys and values of this constants
    :param module: source module
    :return: constants values in dictionary
    """
    module_dict = module.__dict__
    constants = {}
    for key in module_dict.keys():
        if isinstance(module_dict[key], Number):
            constants[key] = module_dict[key]
    return constants


def get_functions_from_module(module):
    """
    Returns dictionary with functions from module
    :param module: source module
    :return: dictionary with Instances of namedtuple who contained functions from module
    """
    module_dict = module.__dict__
    functions = {}
    for key in module_dict.keys():
        if is_function(module_dict[key]):
            functions[key] = Operation(func=module_dict[key], priority=5, is_unary=True, left_ass=True)

    return functions


def comma(x, y):
    """Create list of arguments from two instance"""
    if type(x) == list:
        return x.append(y)
    else:
        return [x, y]


def get_binary_operators():
    comma_operator = Operation(func=comma, priority=0, is_unary=False, left_ass=True)

    return {
        "//": Operation(func=lambda x, y: x // y, priority=3, is_unary=False, left_ass=True),
        "+": Operation(func=lambda x, y: x + y, priority=1, is_unary=False, left_ass=True),
        "-": Operation(func=lambda x, y: x - y, priority=1, is_unary=False, left_ass=True),
        "*": Operation(func=lambda x, y: x * y, priority=2, is_unary=False, left_ass=True),
        "/": Operation(func=lambda x, y: x / y, priority=2, is_unary=False, left_ass=True),
        "^": Operation(func=lambda x, y: x ** y, priority=4, is_unary=False, left_ass=False),
        "%": Operation(func=lambda x, y: x % y, priority=3, is_unary=False, left_ass=True),
        "**": Operation(func=lambda x, y: x ** y, priority=4, is_unary=False, left_ass=False),
        "==": Operation(func=lambda x, y: x == y, priority=0, is_unary=False, left_ass=True),
        ">=": Operation(func=lambda x, y: x >= y, priority=0, is_unary=False, left_ass=True),
        "<=": Operation(func=lambda x, y: x <= y, priority=0, is_unary=False, left_ass=True),
        "!=": Operation(func=lambda x, y: x != y, priority=0, is_unary=False, left_ass=True),
        ">": Operation(func=lambda x, y: x > y, priority=0, is_unary=False, left_ass=True),
        "<": Operation(func=lambda x, y: x < y, priority=0, is_unary=False, left_ass=True),
        ",": comma_operator,
    }


def get_unary_operators():
    return {
        "+": Operation(func=lambda x: x, priority=6, is_unary=True, left_ass=True),
        "-": Operation(func=lambda x: -x, priority=6, is_unary=True, left_ass=True),
    }


class RPNCalculator(object):
    """
    Usage:
    You can create this class without arguments like this:
    calculator = RPNCalculator()
    or argument that will be the list of modules that will be initialized calculator
    calculator = RPNCalculator("math", "your_module")

    To calculate math expression use method calculate:
    calculator.calculate("1+2*3")
    """

    __constants = {}
    __functions = {}
    __binary_operators = {}
    __unary_operators = {}

    def __init__(self, modules=["math"]):
        self.__init_calculator(modules)

    def __init_calculator(self, modules):
        """
        Initialize calculator by functions and constants
        :param modules: list of modules for calculator initialization
        """
        self.__load_some_globals_functions()

        for module_name in modules:
            module = importlib.import_module(module_name)
            self.__constants.update(get_constants_from_module(module))
            self.__functions.update(get_functions_from_module(module))

        self.__binary_operators.update(get_binary_operators())
        self.__unary_operators.update(get_unary_operators())

    def __load_some_globals_functions(self):
        self.__functions["abs"] = Operation(func=abs, priority=5, is_unary=True, left_ass=True)
        self.__functions["pow"] = Operation(func=pow, priority=5, is_unary=True, left_ass=True)
        self.__functions["round"] = Operation(func=round, priority=5, is_unary=True, left_ass=True)

    def calculate(self, expression):
        """
        Calculate math expression
        :param expression: math expression
        :return: result of calculation
        """
        split_expr = self.__split_expression(expression)
        self.__replace_constant_names_on_numbers(split_expr)

        converted_list = self.__associate_atoms_with_functions(split_expr)
        self.__add_multipication_operator(converted_list)

        rpn_expression = self.__convert_to_RPN_expression(converted_list)
        result = self.__calculate_RPM_expression(rpn_expression)

        return result

    def __split_expression(self, expression):
        """
        Split the expression on atoms
        :param expression: Math expression
        :return: List of atoms
        """
        splitters = self.__binary_operators.keys()

        splitters.append(open_parenthis)
        splitters.append(close_parenthis)
        splitters.sort(cmp=lambda x, y: len(y) - len(x))

        sub_exprs = expression.split(' ')
        split_expr = []
        for sub_expr in sub_exprs:
            for element in splitters:
                sub_expr = sub_expr.replace(element, ' ' + element + ' ')
            sub_split_expr = self.__create_list_without_whitespaces(sub_expr.split(' '))
            sub_split_expr = self.__correct_operators(sub_split_expr)
            split_expr = split_expr + sub_split_expr

        return split_expr

    def __correct_operators(self, split_expression):
        split_expression = self.__correct_multi_symbol_operator(split_expression, left_part='/', right_part='/')
        split_expression = self.__correct_multi_symbol_operator(split_expression, left_part='*', right_part='*')
        split_expression = self.__correct_multi_symbol_operator(split_expression, left_part='>', right_part='=')
        split_expression = self.__correct_multi_symbol_operator(split_expression, left_part='<', right_part='=')

        return self.__create_list_without_whitespaces(split_expression)

    def __correct_multi_symbol_operator(self, split_expression, left_part, right_part):
        """
        Function is needed to correct erroneous separation of atomic operator
        :param split_expression: List of expression for correction
        :param left_part: left symbol of operator
        :param right_part: right symbol of operator
        :return: corrected list of atoms
        """
        index = 1
        for _ in range(len(split_expression) - 1):
            if split_expression[index] == right_part and split_expression[index - 1] == left_part:
                split_expression[index] = left_part + right_part
                split_expression[index - 1] = " "
            index += 1

        return split_expression

    def __create_list_without_whitespaces(self, split_expression):
        """
        :param split_expression: List of atoms
        :return: List of atoms without whitespaces
        """
        expr = []
        for atom in split_expression:
            is_whitespace = atom in whitespace or len(atom) == 0
            if not is_whitespace:
                expr.append(atom)

        return expr

    def __replace_constant_names_on_numbers(self, split_expression):
        """
        Replace constants in list of atoms on numbers
        """
        index = 0
        for atom in split_expression:
            if atom in self.__constants:
                split_expression[index] = self.__constants[atom]
            index += 1

    def __associate_atoms_with_functions(self, split_expression):
        """
        Associate strings with constants, functions, operators and parentheses
        :param split_expression: source list with strings
        :return: list with associated atoms
        """
        if type(split_expression) != list:
            raise TypeError()

        list_fot_convert = []
        prev_atom = None

        for atom in split_expression:
            is_binary_operator = (prev_atom == close_parenthis) or (is_number(prev_atom))

            if is_number(atom):
                list_fot_convert.append(float(atom))
            elif atom in self.__unary_operators and not is_binary_operator:
                list_fot_convert.append(self.__unary_operators[atom])
            elif atom in self.__binary_operators:
                list_fot_convert.append(self.__binary_operators[atom])
            elif atom in self.__functions:
                list_fot_convert.append(self.__functions[atom])
            elif atom == open_parenthis or atom == close_parenthis:
                list_fot_convert.append(atom)
            else:
                raise SyntaxError("Expression error. Unknown atom: {0}".format(str(atom)))

            prev_atom = atom

        return list_fot_convert

    def __add_multipication_operator(self, split_expression):
        prev_atom = None
        index = 0
        for atom in split_expression:
            if atom == open_parenthis and type(prev_atom) == float:
                split_expression.insert(index, self.__binary_operators['*'])
            prev_atom = atom
            index += 1

    def __convert_to_RPN_expression(self, split_expression):
        binary_oper_list = self.__binary_operators.values()
        unary_oper_list = self.__unary_operators.values()
        functions_list = self.__functions.values()

        output_stack = []
        oper_stack = []

        for atom in split_expression:
            if type(atom) == float:
                output_stack.append(atom)

            elif atom in functions_list:
                oper_stack.append(atom)

            elif atom == open_parenthis:
                oper_stack.append(atom)

            elif atom == close_parenthis:
                while oper_stack[-1] != open_parenthis:
                    output_stack.append(oper_stack.pop())
                oper_stack.pop()
                if len(oper_stack) != 0 and oper_stack[-1].is_unary:
                    output_stack.append(oper_stack.pop())

            elif atom in unary_oper_list or atom in binary_oper_list:
                while len(oper_stack) != 0 and oper_stack[-1] != open_parenthis and len(output_stack) != 0:
                    if atom.priority < oper_stack[-1].priority:
                        condition_for_break = False
                    elif atom.priority > oper_stack[-1].priority:
                        condition_for_break = True
                    elif atom.priority == oper_stack[-1].priority and not atom.left_ass:
                        condition_for_break = True
                    else:
                        condition_for_break = False

                    if condition_for_break:
                        break

                    output_stack.append(oper_stack.pop())

                oper_stack.append(atom)

            else:
                raise Exception("Invalid operation")

        while len(oper_stack) != 0:
            output_stack.append(oper_stack.pop())

        return output_stack

    def __calculate_RPM_expression(self, rpn_expression):
        number_stack = []
        binary_oper_list = self.__binary_operators.values()
        unary_oper_list = self.__unary_operators.values()
        functions_list = self.__functions.values()

        rpn_expression = rpn_expression[::-1]

        while len(rpn_expression) != 0:
            atom = rpn_expression.pop()
            
            if type(atom) == float:
                number_stack.append(atom)

            elif atom in functions_list or atom in unary_oper_list:
                if len(number_stack) < 1:
                    raise Exception("Invalid expression.")
                arg1 = number_stack.pop()
                if type(arg1) == list:
                    result = atom.func(*arg1)
                else:
                    result = atom.func(arg1)
                number_stack.append(result)

            elif atom in binary_oper_list:
                if len(number_stack) < 2:
                    raise Exception("Invalid expression.")
                arg1 = number_stack.pop()
                arg2 = number_stack.pop()
                result = atom.func(arg2, arg1)
                number_stack.append(result)

        if len(number_stack) != 1:
            raise Exception("Invalid expression")
        return number_stack[0]
