#!/usr/bin/env python2
import rpn
from sys import argv

def evalute():
    if len(argv) == 1:
        print "Missing expression"
    else:
        calculator = rpn.RPNCalculator()
        try:
            result = calculator.calculate(argv[1])
            print "Result: {}".format(result)
        except Exception as e:
            print "Error. Error information: {}".format(e.message)

if __name__ == '__main__':
    evalute()
